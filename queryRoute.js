const express = require ('express')
const {buildQuery} = require("../services/queryService");

const queryRouter = express.Router();

queryRouter.post('/', async (req, res)=>{
    try{
        const {field_id} = req.body;
        if(field_id.length===0){
            res.status(400).json({message:"No fields id"})
        }
        const query = await buildQuery(field_id)
        if(query.length===0){
            res.status(400).json({message:"Query cannot be build"})
        }
        res.status(200).json({query})
    }catch (e){
        res.status(500).json({message:"Server ERROR " + e})
    }
})


module.exports = {queryRouter}