const redis = require('redis');
const redisPort = 6379

const client = redis.createClient(redisPort);

client.on("error", (err) => {
    console.log(err);
});


const getCacheById = (key) => {
    return new Promise((res, rej) => {
        client.get(key, (err, reply) => {
            res(reply);
        });
    })

}

const addCache = async (data)=>{
    for (let i = 0; i < data.length; i++) {
        await client.set(i, JSON.stringify(data[i]))
    }
}

const addCacheFilter = async (counter, data)=>{
        await  client.set("f"+counter, JSON.stringify(data))
}

module.exports = {getCacheById, addCache, addCacheFilter}