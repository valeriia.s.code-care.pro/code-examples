const {getData} = require("./getService");
const buildQuery = async (field_id) => {
    try{
        const data = await getData()
        let fields =[]
        let query ="select "
        data.forEach(el=>{
            if(field_id.includes(el.FIELDID)){
                fields.push(el)
            }
        })
        fields.forEach((el, key)=>{
            if(key===fields.length-1){
                query += el.FIELDNAME+" "
            }
            else{
                query += el.FIELDNAME+", "
            }
        })
        query+="from DELTA_ALPHA a "
        fields.forEach((el, key)=>{
            if(key===0){
                query+= "inner join "+el.OBJECTNAME+" "+el.OBJECTALIAS
                query+= " on "+el.PRIMARYKEY+" = "+el.ALPHAJOINFIELD
            }
            if(!query.includes(el.OBJECTNAME)){
                query+= " inner join "+el.OBJECTNAME+" "+el.OBJECTALIAS
                query+= " on "+el.PRIMARYKEY+" = "+el.ALPHAJOINFIELD
            }
        })
        query+=";"
        return query;

    }catch (e){
        console.log(e);
    }
}

module.exports = {buildQuery}