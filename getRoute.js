const express = require("express")
const {getFilterable, getDimensionable, getExpresionable, getFilterValueSelect} = require("../services/getService");


const getRouter = express.Router();

getRouter.get('/filters', async(req, res)=>{
    try{
        let data = await getFilterable()
        if(data.length === 0){
            res.status(400).json({message:"No filterable data"})
        }
        res.status(200).json({data})
    }catch (e){
        res.status(500).json({message:"Server ERROR " + e})
    }
})

getRouter.get('/dimensions', async (req, res)=>{
    try{
        let data = await getDimensionable();
        if(data.length === 0){
            res.status(400).json({message:"No dimensionable data"})
        }
        res.status(200).json({data})
    }catch (e){
        res.status(500).json({message: "Server ERROR "+ e})
    }
})

getRouter.get('/expressions', async (req, res)=>{
    try{
        let data = await getExpresionable();
        if(data.length === 0){
            res.status(400).json({message:"No expressionable data"})
        }
        res.status(200).json({data})
    }catch (e){
        res.status(500).json({message: "Server ERROR "+ e})
    }
})
//req.params

getRouter.get('/filtervalues/:id', async (req, res)=>{
    try{
        const key = req.params.id;
        let data = await getFilterValueSelect(key);
        if(data.length === 0){
            res.status(400).json({message:"No filter value select"})
        }
        res.status(200).json({data})
    }catch (e){
        res.status(500).json({message: "Server ERROR "+ e})
    }
})

module.exports = {getRouter};

