const {getCacheById} = require('../cache/redisCache');

let length = 0

const setLength = (l)=>{
    length = l
}

const getData = async  () => {
    let data = []
    for(let i=0; i<length; i++){
        await getCacheById(i).then((value => data.push(JSON.parse(value))))
    }
    return data
}

const getFilterable = async ()=>{
    try{
        let data = await getData()
        return data.filter((el)=>{
            return el.FILTERABLE === true;
        })
    }catch (e){
        return e;
    }
}

const getDimensionable = async ()=>{
    try{
        let data = await getData();
        return data.filter((el)=>{
            return el.DIMENSIONABLE === true
        })
    }catch (e){
        return (e)
    }
}

const getExpresionable = async ()=>{
    try{
        let data = await getData();
        return data.filter((el)=>{
            return el.EXPRESSIONABLE === true
        })
    }catch (e){
        return (e)
    }
}

const getFilterValueSelect = async (key)=>{
    let data = []
    for(let i=key; i<=key; i++){
        await getCacheById("f"+i).then((value => data.push(JSON.parse(value))))
    }
    return data
}



module.exports = {getData, setLength, getFilterable, getDimensionable, getExpresionable, getFilterValueSelect}